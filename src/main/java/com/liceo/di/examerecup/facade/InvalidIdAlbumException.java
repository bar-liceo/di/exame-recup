package com.liceo.di.examerecup.facade;


public class InvalidIdAlbumException extends Exception {

    public InvalidIdAlbumException(){}

    public InvalidIdAlbumException(String message){
        super(message);
    }

    public InvalidIdAlbumException(Throwable cause){
        super(cause);
    }

    public InvalidIdAlbumException(String message, Throwable cause){
        super(message, cause);
    }
}
