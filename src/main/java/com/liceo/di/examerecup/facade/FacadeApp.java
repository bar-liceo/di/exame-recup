package com.liceo.di.examerecup.facade;


import com.liceo.di.examerecup.album.Album;
import com.liceo.di.examerecup.album.Format;
import com.liceo.di.examerecup.album.Style;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.*;

public class FacadeApp implements Serializable {

    private static FacadeApp instance = new FacadeApp();
    private Map<Integer, Album> AlbumList;
    private Integer idCount;

    /**
     * Constructor (private) in order to implement Singleton Pattern,
     * the constructor initialize the Application instance with some example data.
     */
    private FacadeApp() {
        this.initialiseApp();
    }

    /**
     * Get an instance of the Application.
     *
     * @return the instance of Application
     */
    public static FacadeApp getAppInstance() {
        return instance;
    }

    /**
     * Initialize the Application instance.
     * You only need this if you like reinitialize all the data.
     */
    public void initialiseApp() {
        this.AlbumList = new HashMap<Integer, Album>();
        this.idCount = 0;
        loadExampleData();
    }


    /**
     * Gets the whole Album List.
     *
     * @return the Album List, if there is no Album or no album list the List returned will be empty
     */
    public List<Album> getAlbumList() {
        if (this.AlbumList == null) {
            return new ArrayList<>();
        }
        return new ArrayList<Album>(this.AlbumList.values());
    }

    /**
     * Filters the Album by title field.
     *
     * @param pattern the substring that the Album should match
     * @return the List with the Album that match the pattern given
     */
    public List<Album> filterAlbumByTitle(String pattern) {
        List<Album> result = new ArrayList<>();
        for (Album album : this.AlbumList.values()) {
            if (album.getTitle().toLowerCase().contains(pattern.toLowerCase())) {
                result.add(album);
            }
        }
        return result;
    }

    /**
     * Filters the Album by artist field.
     *
     * @param pattern the substring that the Album should match
     * @return the List with the Album that match the pattern given
     */
    public List<Album> filterAlbumByArtist(String pattern) {
        List<Album> result = new ArrayList<>();
        for (Album album : this.AlbumList.values()) {
            if (album.getArtist().toLowerCase().contains(pattern.toLowerCase())) {
                result.add(album);
            }
        }
        return result;
    }

    /**
     * Filters the Album by style field.
     *
     * @param styles the List of Style that the Album should have
     * @return the List with the Album that match the pattern given
     */
    public List<Album> filterAlbumByStyle(List<Style> styles) {
        List<Album> result = new ArrayList<>();
        for (Album album : this.AlbumList.values()) {
            if (album.getStyle().containsAll(styles)) {
                result.add(album);
            }
        }
        return result;
    }

    /**
     * Finds and Album by id
     *
     * @param id the id of the Album to retrieve
     * @return the Album with the id given or null if the id does not exist
     */
    public Album findAlbumById(Integer id) {
        return this.AlbumList.get(id);
    }

    /**
     * Adds a Books object to the Album list with the proper id parameter
     * this function use the same number as key in the Album Map.
     *
     * @param album to be added
     * @return the id of new album added
     */
    public Integer addAlbum(Album album) {
        Integer id = this.idCount++;
        album.setId(id);
        this.AlbumList.put(id, album);
        return id;
    }

    /**
     * Edits the information of an Album. It is important that you do not edit the Id parameter of Album object.
     *
     * @param editedAlbum Album object wich contains the new information of an Album object
     * @throws InvalidIdAlbumException when the Id of editedAlbum does not exist on Album List
     */
    public void editAlbum(Album editedAlbum) throws InvalidIdAlbumException {
        if (this.AlbumList.get(editedAlbum.getId()) == null) {
            throw new InvalidIdAlbumException("There aren't an Album with the same id when trying to edit");
        }
        this.AlbumList.put(editedAlbum.getId(), editedAlbum);
    }

    /**
     * Deletes the Album which have the Id given.
     *
     * @param idAlbumToDelete the Id of the Album to delete from Album List
     * @throws InvalidIdAlbumException when idAlbumToDelete does not exist on Album List
     */
    public void deleteAlbum(int idAlbumToDelete) throws InvalidIdAlbumException {
        if (this.AlbumList.get(idAlbumToDelete) == null) {
            throw new InvalidIdAlbumException("There aren't an Album with the same id when trying to delete");
        }
        this.AlbumList.remove(idAlbumToDelete);
    }

    /**
     * Adds styles to an Album giving the album id.
     *
     * @param idAlbum id of Album to add styles
     * @param styles  the List of styles will be added
     * @throws InvalidIdAlbumException when the id Album does not exist on Album List
     */
    public void addStylesToAlbum(int idAlbum, Set<Style> styles) throws InvalidIdAlbumException {
        //TODO: Change the name of method
        Album album = this.AlbumList.get(idAlbum);
        if (album == null) {
            throw new InvalidIdAlbumException("There aren't an Album with the same id when trying to add styles");
        }
        album.addStyles(styles);
    }

    /**
     * Loads into the Album list some example data.
     */
    private void loadExampleData() {
        //add data
        addAlbum(new Album("The Number Of The Beast", "Iron Maiden", 8, null, Format.VINYL, LocalDate.ofYearDay(1982,
                1), new HashSet<Style>(Arrays.asList(Style.METAL, Style.ROCK))));
        addAlbum(new Album("Abbey Road", "The Beatles", 17, null, Format.VINYL, LocalDate.ofYearDay(1969,
                1), new HashSet<Style>(Arrays.asList(Style.POP, Style.ROCK))));
        addAlbum(new Album("Gwendal", "Gwendal", 14, null, Format.CD, LocalDate.ofYearDay(1974,
                1), new HashSet<Style>(Arrays.asList(Style.FOLK))));

        addAlbum(new Album("Master Of Puppets", "Metallica", 8, "Elektra", Format.CD, LocalDate.ofYearDay(1986,
                1), new HashSet<Style>(Arrays.asList(Style.ROCK, Style.METAL))));
        addAlbum(new Album("Master Of Puppets", "Metallica", 8, "Elektra", Format.CASSETTE, LocalDate.ofYearDay(1986,
                1), new HashSet<Style>(Arrays.asList(Style.ROCK, Style.METAL))));
        addAlbum(new Album("Master Of Puppets", "Metallica", 8, "Elektra", Format.VINYL, LocalDate.ofYearDay(1986,
                1), new HashSet<Style>(Arrays.asList(Style.ROCK, Style.METAL))));

        addAlbum(new Album("Dónde Están Los Ladrones?", "Shakira", 11, null, Format.CD,
                LocalDate.ofYearDay(1998,
                        1), new HashSet<Style>(Arrays.asList(Style.POP))));

        addAlbum(new Album("Grandes Éxitos", "Los Secretos", 20, "DRO", Format.CD,
                LocalDate.ofYearDay(1996,
                        1), new HashSet<Style>(Arrays.asList(Style.POP))));

        addAlbum(new Album("Blue Train", "John Coltrane", 5, null, Format.VINYL,
                LocalDate.ofYearDay(1957,
                        1), new HashSet<Style>(Arrays.asList(Style.JAZZ))));

        addAlbum(new Album("Time In", "Dave Brubeck", 8, null, Format.CD,
                LocalDate.ofYearDay(1966,
                        1), new HashSet<Style>(Arrays.asList(Style.JAZZ))));
        addAlbum(new Album("London Calling", "The Clash", 19, null, Format.CD,
                LocalDate.ofYearDay(1979,
                        1), new HashSet<Style>(Arrays.asList(Style.PUNK))));
    }
}
