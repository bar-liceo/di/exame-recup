package com.liceo.di.examerecup.gui;

import com.liceo.di.examerecup.album.Album;
import com.liceo.di.examerecup.facade.FacadeApp;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class HelloApplication extends Application {

    // You shoud place this line of code in the Class
    // where you like to use the FacadeApp
    FacadeApp fApp = FacadeApp.getAppInstance();
    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("hello-view.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 320, 240);
        stage.setTitle("Hello!");
        stage.setScene(scene);
        stage.show();

        // An example of how to use the FacadeApp Class
        // In this case how to get the Album List
        System.out.println("--- Album List from App ---");
        System.out.println(fApp.getAlbumList());
        System.out.println("--------------------------");

        // Now it adds a dummy book
        fApp.addAlbum(new Album("Title Dummy App", "Artist Dummy App"));
        // if you see the terminal you can see that the List are the same

    }

    public static void main(String[] args) {
        launch();
    }
}