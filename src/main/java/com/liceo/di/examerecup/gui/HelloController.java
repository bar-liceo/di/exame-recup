package com.liceo.di.examerecup.gui;

import com.liceo.di.examerecup.album.Album;
import com.liceo.di.examerecup.facade.FacadeApp;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class HelloController {
    @FXML
    private Label welcomeText;
    // You shoud place this line of code in the Class
    // where you like to use the FacadeApp
    FacadeApp fApp = FacadeApp.getAppInstance();
    // The instance of FacadeApp will be the same in App than in Controller

    @FXML
    protected void onHelloButtonClick() {
        welcomeText.setText("Welcome to JavaFX Application!");

        // An example of how to use the FacadeApp Class

        fApp.addAlbum(new Album("Title Dummy Controller", "Artist Dummy Controller"));

        // Now you can see that it prints the same list than in the App Class
        // but, now with the other Dummy Album added
        System.out.println("--- Album List from Controller ---");
        System.out.println(fApp.getAlbumList());
        System.out.println("---------------------------------");
    }
}