package com.liceo.di.examerecup.album;


import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

public class Album {
    private String title;
    private String artist;
    private int numberOfTracks;
    private String label;
    private Format format;
    private LocalDate datePublished;
    private Set<Style> style;
    private int id;

    public Album() {
        this.style = new HashSet<>();
        this.id = 0;
    }

    public Album(String title, String artist) {
        this();
        this.artist = artist;
        this.title = title;
    }

    public Album(String title, String artist, LocalDate datePublished) {
        this();
        this.title = title;
        this.artist = artist;
        this.datePublished = datePublished;
    }

    public Album(String title, String artist, int numberOfTracks, LocalDate datePublished) {
        this();
        this.title = title;
        this.artist = artist;
        this.numberOfTracks = numberOfTracks;
        this.datePublished = datePublished;
    }

    public Album(String title, String artist, int numberOfTracks, String label, Format format, LocalDate datePublished, Set<Style> style) {
        this();
        this.title = title;
        this.artist = artist;
        this.numberOfTracks = numberOfTracks;
        this.label = label;
        this.format = format;
        this.datePublished = datePublished;
        if (style != null){
            this.style.addAll(style);
        }
    }

    public int getNumberOfTracks() {
        return numberOfTracks;
    }

    public void setNumberOfTracks(int numberOfTracks) {
        this.numberOfTracks = numberOfTracks;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Format getFormat() {
        return format;
    }

    public void setFormat(Format format) {
        this.format = format;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDate getDatePublished() {
        return datePublished;
    }

    public void setDatePublished(LocalDate datePublished) {
        this.datePublished = datePublished;
    }

    public Set<Style> getStyle() {
        return style;
    }

    public void setStyle(Set<Style> style) {
        this.style = style;
    }

    public void addStyle(Style style) {
        if (style != null) {
            this.style.add(style);
        }
    }

    public void addStyles(Set<Style> styles) {
        if (styles != null) {
            this.style.addAll(styles);
        }
    }

    @Override
    public String toString() {
        return "Album{" + id + " " +
                "title='" + title + '\'' +
                ", artist='" + artist + '\'' +
                ", numberOfTracks=" + numberOfTracks +
                ", label='" + label + '\'' +
                ", format=" + format +
                ", datePublished=" + datePublished +
                ", style=" + style +
                "}\n";
    }
}
