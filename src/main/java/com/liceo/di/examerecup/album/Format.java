package com.liceo.di.examerecup.album;

public enum Format {
    CD,
    VINYL,
    CASSETTE,
    DIGITAL
}
