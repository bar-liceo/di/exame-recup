package com.liceo.di.examerecup.album;

public enum Style {
    ROCK,
    CLASSICAL,
    FOLK,
    POP,
    BLUES,
    COUNTRY,
    ELECTRONIC,
    JAZZ,
    METAL,
    PUNK
}
