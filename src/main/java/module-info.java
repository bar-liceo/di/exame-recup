module com.liceo.di.examerecup.gui {
    requires javafx.controls;
    requires javafx.fxml;


    exports com.liceo.di.examerecup.gui;
    exports com.liceo.di.examerecup.album;
    exports com.liceo.di.examerecup.facade;
    opens com.liceo.di.examerecup.gui to javafx.fxml;
}